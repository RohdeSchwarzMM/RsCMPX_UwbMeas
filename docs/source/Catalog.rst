Catalog
----------------------------------------





.. autoclass:: RsCMPX_UwbMeas.Implementations.Catalog.CatalogCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.catalog.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Catalog_UwbMeas.rst