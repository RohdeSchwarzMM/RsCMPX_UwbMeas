UwbMeas
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CATalog:UWB:MEAS<instance>:SPATh

.. code-block:: python

	CATalog:UWB:MEAS<instance>:SPATh



.. autoclass:: RsCMPX_UwbMeas.Implementations.Catalog.UwbMeas.UwbMeasCls
	:members:
	:undoc-members:
	:noindex: