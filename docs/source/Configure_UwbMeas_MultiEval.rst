MultiEval
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:UWB:MEASurement<Instance>:MEValuation:PTRacking
	single: CONFigure:UWB:MEASurement<Instance>:MEValuation:PMODe
	single: CONFigure:UWB:MEASurement<Instance>:MEValuation:SCONdition
	single: CONFigure:UWB:MEASurement<Instance>:MEValuation:TOUT
	single: CONFigure:UWB:MEASurement<Instance>:MEValuation:SCOunt
	single: CONFigure:UWB:MEASurement<Instance>:MEValuation:REPetition
	single: CONFigure:UWB:MEASurement<Instance>:MEValuation:MOEXception
	single: CONFigure:UWB:MEASurement<Instance>:MEValuation:CAPLength
	single: CONFigure:UWB:MEASurement<Instance>:MEValuation:EOFFset

.. code-block:: python

	CONFigure:UWB:MEASurement<Instance>:MEValuation:PTRacking
	CONFigure:UWB:MEASurement<Instance>:MEValuation:PMODe
	CONFigure:UWB:MEASurement<Instance>:MEValuation:SCONdition
	CONFigure:UWB:MEASurement<Instance>:MEValuation:TOUT
	CONFigure:UWB:MEASurement<Instance>:MEValuation:SCOunt
	CONFigure:UWB:MEASurement<Instance>:MEValuation:REPetition
	CONFigure:UWB:MEASurement<Instance>:MEValuation:MOEXception
	CONFigure:UWB:MEASurement<Instance>:MEValuation:CAPLength
	CONFigure:UWB:MEASurement<Instance>:MEValuation:EOFFset



.. autoclass:: RsCMPX_UwbMeas.Implementations.Configure.UwbMeas.MultiEval.MultiEvalCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.uwbMeas.multiEval.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_UwbMeas_MultiEval_Modulation.rst
	Configure_UwbMeas_MultiEval_MprFrequency.rst
	Configure_UwbMeas_MultiEval_Phr.rst
	Configure_UwbMeas_MultiEval_PhrRate.rst
	Configure_UwbMeas_MultiEval_Pmask.rst
	Configure_UwbMeas_MultiEval_Ppdu.rst
	Configure_UwbMeas_MultiEval_PpLength.rst
	Configure_UwbMeas_MultiEval_PrfMode.rst
	Configure_UwbMeas_MultiEval_Psdu.rst
	Configure_UwbMeas_MultiEval_PsFormat.rst
	Configure_UwbMeas_MultiEval_Result.rst
	Configure_UwbMeas_MultiEval_Spectrum.rst
	Configure_UwbMeas_MultiEval_StSegments.rst
	Configure_UwbMeas_MultiEval_StsGap.rst
	Configure_UwbMeas_MultiEval_StsLength.rst
	Configure_UwbMeas_MultiEval_TsMask.rst