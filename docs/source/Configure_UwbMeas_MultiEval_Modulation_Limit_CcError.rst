CcError
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:UWB:MEASurement<Instance>:MEValuation:MODulation:LIMit:CCERor

.. code-block:: python

	CONFigure:UWB:MEASurement<Instance>:MEValuation:MODulation:LIMit:CCERor



.. autoclass:: RsCMPX_UwbMeas.Implementations.Configure.UwbMeas.MultiEval.Modulation.Limit.CcError.CcErrorCls
	:members:
	:undoc-members:
	:noindex: