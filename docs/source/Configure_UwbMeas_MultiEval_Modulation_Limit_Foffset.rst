Foffset
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:UWB:MEASurement<Instance>:MEValuation:MODulation:LIMit:FOFFset

.. code-block:: python

	CONFigure:UWB:MEASurement<Instance>:MEValuation:MODulation:LIMit:FOFFset



.. autoclass:: RsCMPX_UwbMeas.Implementations.Configure.UwbMeas.MultiEval.Modulation.Limit.Foffset.FoffsetCls
	:members:
	:undoc-members:
	:noindex: