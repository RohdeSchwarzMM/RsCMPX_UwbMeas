PmlWidth
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:UWB:MEASurement<Instance>:MEValuation:MODulation:LIMit:PMLWidth

.. code-block:: python

	CONFigure:UWB:MEASurement<Instance>:MEValuation:MODulation:LIMit:PMLWidth



.. autoclass:: RsCMPX_UwbMeas.Implementations.Configure.UwbMeas.MultiEval.Modulation.Limit.PmlWidth.PmlWidthCls
	:members:
	:undoc-members:
	:noindex: