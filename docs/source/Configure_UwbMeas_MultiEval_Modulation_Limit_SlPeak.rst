SlPeak
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:UWB:MEASurement<Instance>:MEValuation:MODulation:LIMit:SLPeak

.. code-block:: python

	CONFigure:UWB:MEASurement<Instance>:MEValuation:MODulation:LIMit:SLPeak



.. autoclass:: RsCMPX_UwbMeas.Implementations.Configure.UwbMeas.MultiEval.Modulation.Limit.SlPeak.SlPeakCls
	:members:
	:undoc-members:
	:noindex: