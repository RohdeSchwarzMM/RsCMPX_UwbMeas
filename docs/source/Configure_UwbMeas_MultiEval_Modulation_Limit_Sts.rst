Sts
----------------------------------------





.. autoclass:: RsCMPX_UwbMeas.Implementations.Configure.UwbMeas.MultiEval.Modulation.Limit.Sts.StsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.uwbMeas.multiEval.modulation.limit.sts.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_UwbMeas_MultiEval_Modulation_Limit_Sts_Nrmse.rst