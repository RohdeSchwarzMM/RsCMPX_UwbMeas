Nrmse
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:UWB:MEASurement<Instance>:MEValuation:MODulation:LIMit:STS:NRMSe

.. code-block:: python

	CONFigure:UWB:MEASurement<Instance>:MEValuation:MODulation:LIMit:STS:NRMSe



.. autoclass:: RsCMPX_UwbMeas.Implementations.Configure.UwbMeas.MultiEval.Modulation.Limit.Sts.Nrmse.NrmseCls
	:members:
	:undoc-members:
	:noindex: