MprFrequency
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:UWB:MEASurement<Instance>:MEValuation:MPRFrequency<Record>

.. code-block:: python

	CONFigure:UWB:MEASurement<Instance>:MEValuation:MPRFrequency<Record>



.. autoclass:: RsCMPX_UwbMeas.Implementations.Configure.UwbMeas.MultiEval.MprFrequency.MprFrequencyCls
	:members:
	:undoc-members:
	:noindex: