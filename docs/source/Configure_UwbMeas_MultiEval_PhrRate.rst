PhrRate
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:UWB:MEASurement<Instance>:MEValuation:PHRRate<Record>

.. code-block:: python

	CONFigure:UWB:MEASurement<Instance>:MEValuation:PHRRate<Record>



.. autoclass:: RsCMPX_UwbMeas.Implementations.Configure.UwbMeas.MultiEval.PhrRate.PhrRateCls
	:members:
	:undoc-members:
	:noindex: