Bitrate<Record>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr20
	rc = driver.configure.uwbMeas.multiEval.phr.bitrate.repcap_record_get()
	driver.configure.uwbMeas.multiEval.phr.bitrate.repcap_record_set(repcap.Record.Nr1)



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:UWB:MEASurement<Instance>:MEValuation:PHR:BITRate<Record>

.. code-block:: python

	CONFigure:UWB:MEASurement<Instance>:MEValuation:PHR:BITRate<Record>



.. autoclass:: RsCMPX_UwbMeas.Implementations.Configure.UwbMeas.MultiEval.Phr.Bitrate.BitrateCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.uwbMeas.multiEval.phr.bitrate.clone()