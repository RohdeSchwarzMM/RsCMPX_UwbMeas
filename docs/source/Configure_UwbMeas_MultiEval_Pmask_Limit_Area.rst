Area
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:UWB:MEASurement<Instance>:MEValuation:PMASk:LIMit:AREA

.. code-block:: python

	CONFigure:UWB:MEASurement<Instance>:MEValuation:PMASk:LIMit:AREA



.. autoclass:: RsCMPX_UwbMeas.Implementations.Configure.UwbMeas.MultiEval.Pmask.Limit.Area.AreaCls
	:members:
	:undoc-members:
	:noindex: