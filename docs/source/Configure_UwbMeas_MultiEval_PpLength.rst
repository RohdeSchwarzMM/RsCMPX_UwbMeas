PpLength
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:UWB:MEASurement<Instance>:MEValuation:PPLength<Record>

.. code-block:: python

	CONFigure:UWB:MEASurement<Instance>:MEValuation:PPLength<Record>



.. autoclass:: RsCMPX_UwbMeas.Implementations.Configure.UwbMeas.MultiEval.PpLength.PpLengthCls
	:members:
	:undoc-members:
	:noindex: