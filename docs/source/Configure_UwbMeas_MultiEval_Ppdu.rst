Ppdu
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:UWB:MEASurement<Instance>:MEValuation:PPDU:RECords
	single: CONFigure:UWB:MEASurement<Instance>:MEValuation:PPDU:SRECord
	single: CONFigure:UWB:MEASurement<Instance>:MEValuation:PPDU:NUMBer

.. code-block:: python

	CONFigure:UWB:MEASurement<Instance>:MEValuation:PPDU:RECords
	CONFigure:UWB:MEASurement<Instance>:MEValuation:PPDU:SRECord
	CONFigure:UWB:MEASurement<Instance>:MEValuation:PPDU:NUMBer



.. autoclass:: RsCMPX_UwbMeas.Implementations.Configure.UwbMeas.MultiEval.Ppdu.PpduCls
	:members:
	:undoc-members:
	:noindex: