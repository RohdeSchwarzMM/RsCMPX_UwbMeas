PrfMode
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:UWB:MEASurement<Instance>:MEValuation:PRFMode<Record>

.. code-block:: python

	CONFigure:UWB:MEASurement<Instance>:MEValuation:PRFMode<Record>



.. autoclass:: RsCMPX_UwbMeas.Implementations.Configure.UwbMeas.MultiEval.PrfMode.PrfModeCls
	:members:
	:undoc-members:
	:noindex: