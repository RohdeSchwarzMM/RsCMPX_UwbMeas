PsFormat
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:UWB:MEASurement<Instance>:MEValuation:PSFormat<Record>

.. code-block:: python

	CONFigure:UWB:MEASurement<Instance>:MEValuation:PSFormat<Record>



.. autoclass:: RsCMPX_UwbMeas.Implementations.Configure.UwbMeas.MultiEval.PsFormat.PsFormatCls
	:members:
	:undoc-members:
	:noindex: