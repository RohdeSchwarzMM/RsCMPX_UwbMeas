Psdu
----------------------------------------





.. autoclass:: RsCMPX_UwbMeas.Implementations.Configure.UwbMeas.MultiEval.Psdu.PsduCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.uwbMeas.multiEval.psdu.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_UwbMeas_MultiEval_Psdu_Bitrate.rst