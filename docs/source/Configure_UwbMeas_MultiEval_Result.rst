Result
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:UWB:MEASurement<Instance>:MEValuation:RESult:TSMask
	single: CONFigure:UWB:MEASurement<Instance>:MEValuation:RESult:PVTime
	single: CONFigure:UWB:MEASurement<Instance>:MEValuation:RESult:EMODulation
	single: CONFigure:UWB:MEASurement<Instance>:MEValuation:RESult:DDECoding

.. code-block:: python

	CONFigure:UWB:MEASurement<Instance>:MEValuation:RESult:TSMask
	CONFigure:UWB:MEASurement<Instance>:MEValuation:RESult:PVTime
	CONFigure:UWB:MEASurement<Instance>:MEValuation:RESult:EMODulation
	CONFigure:UWB:MEASurement<Instance>:MEValuation:RESult:DDECoding



.. autoclass:: RsCMPX_UwbMeas.Implementations.Configure.UwbMeas.MultiEval.Result.ResultCls
	:members:
	:undoc-members:
	:noindex: