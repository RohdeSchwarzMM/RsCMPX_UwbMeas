Spectrum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:UWB:MEASurement<Instance>:MEValuation:SPECtrum:SCOunt
	single: CONFigure:UWB:MEASurement<Instance>:MEValuation:SPECtrum:MSPLength

.. code-block:: python

	CONFigure:UWB:MEASurement<Instance>:MEValuation:SPECtrum:SCOunt
	CONFigure:UWB:MEASurement<Instance>:MEValuation:SPECtrum:MSPLength



.. autoclass:: RsCMPX_UwbMeas.Implementations.Configure.UwbMeas.MultiEval.Spectrum.SpectrumCls
	:members:
	:undoc-members:
	:noindex: