StSegments
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:UWB:MEASurement<Instance>:MEValuation:STSegments<Record>

.. code-block:: python

	CONFigure:UWB:MEASurement<Instance>:MEValuation:STSegments<Record>



.. autoclass:: RsCMPX_UwbMeas.Implementations.Configure.UwbMeas.MultiEval.StSegments.StSegmentsCls
	:members:
	:undoc-members:
	:noindex: