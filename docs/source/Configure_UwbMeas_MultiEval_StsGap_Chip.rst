Chip
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:UWB:MEASurement<Instance>:MEValuation:STSGap:CHIP<Record>

.. code-block:: python

	CONFigure:UWB:MEASurement<Instance>:MEValuation:STSGap:CHIP<Record>



.. autoclass:: RsCMPX_UwbMeas.Implementations.Configure.UwbMeas.MultiEval.StsGap.Chip.ChipCls
	:members:
	:undoc-members:
	:noindex: