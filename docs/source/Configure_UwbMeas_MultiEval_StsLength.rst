StsLength
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:UWB:MEASurement<Instance>:MEValuation:STSLength<Record>

.. code-block:: python

	CONFigure:UWB:MEASurement<Instance>:MEValuation:STSLength<Record>



.. autoclass:: RsCMPX_UwbMeas.Implementations.Configure.UwbMeas.MultiEval.StsLength.StsLengthCls
	:members:
	:undoc-members:
	:noindex: