Area<Area>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr3
	rc = driver.configure.uwbMeas.multiEval.tsMask.limit.area.repcap_area_get()
	driver.configure.uwbMeas.multiEval.tsMask.limit.area.repcap_area_set(repcap.Area.Nr1)



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:UWB:MEASurement<Instance>:MEValuation:TSMask:LIMit:AREA<nr>

.. code-block:: python

	CONFigure:UWB:MEASurement<Instance>:MEValuation:TSMask:LIMit:AREA<nr>



.. autoclass:: RsCMPX_UwbMeas.Implementations.Configure.UwbMeas.MultiEval.TsMask.Limit.Area.AreaCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.uwbMeas.multiEval.tsMask.limit.area.clone()