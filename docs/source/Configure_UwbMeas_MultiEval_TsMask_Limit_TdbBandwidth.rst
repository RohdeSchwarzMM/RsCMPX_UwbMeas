TdbBandwidth
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:UWB:MEASurement<Instance>:MEValuation:TSMask:LIMit:TDBBandwidth

.. code-block:: python

	CONFigure:UWB:MEASurement<Instance>:MEValuation:TSMask:LIMit:TDBBandwidth



.. autoclass:: RsCMPX_UwbMeas.Implementations.Configure.UwbMeas.MultiEval.TsMask.Limit.TdbBandwidth.TdbBandwidthCls
	:members:
	:undoc-members:
	:noindex: