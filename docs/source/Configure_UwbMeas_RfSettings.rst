RfSettings
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:UWB:MEASurement<Instance>:RFSettings:CHANnel
	single: CONFigure:UWB:MEASurement<Instance>:RFSettings:ENPower
	single: CONFigure:UWB:MEASurement<Instance>:RFSettings:EATTenuation
	single: CONFigure:UWB:MEASurement<Instance>:RFSettings:UMARgin

.. code-block:: python

	CONFigure:UWB:MEASurement<Instance>:RFSettings:CHANnel
	CONFigure:UWB:MEASurement<Instance>:RFSettings:ENPower
	CONFigure:UWB:MEASurement<Instance>:RFSettings:EATTenuation
	CONFigure:UWB:MEASurement<Instance>:RFSettings:UMARgin



.. autoclass:: RsCMPX_UwbMeas.Implementations.Configure.UwbMeas.RfSettings.RfSettingsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.uwbMeas.rfSettings.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_UwbMeas_RfSettings_Frequency.rst