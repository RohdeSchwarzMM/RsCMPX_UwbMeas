Range
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:UWB:MEASurement<Instance>:RFSettings:FREQuency:RANGe

.. code-block:: python

	CONFigure:UWB:MEASurement<Instance>:RFSettings:FREQuency:RANGe



.. autoclass:: RsCMPX_UwbMeas.Implementations.Configure.UwbMeas.RfSettings.Frequency.Range.RangeCls
	:members:
	:undoc-members:
	:noindex: