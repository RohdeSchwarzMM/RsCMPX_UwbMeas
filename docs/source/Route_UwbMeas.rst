UwbMeas
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:UWB:MEAS<instance>:SPATh

.. code-block:: python

	ROUTe:UWB:MEAS<instance>:SPATh



.. autoclass:: RsCMPX_UwbMeas.Implementations.Route.UwbMeas.UwbMeasCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.route.uwbMeas.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Route_UwbMeas_RfSettings.rst