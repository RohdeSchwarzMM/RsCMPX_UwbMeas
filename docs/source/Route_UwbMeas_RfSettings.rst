RfSettings
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:UWB:MEASurement<Instance>:RFSettings:CONNector

.. code-block:: python

	ROUTe:UWB:MEASurement<Instance>:RFSettings:CONNector



.. autoclass:: RsCMPX_UwbMeas.Implementations.Route.UwbMeas.RfSettings.RfSettingsCls
	:members:
	:undoc-members:
	:noindex: