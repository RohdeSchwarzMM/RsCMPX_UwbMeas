RsCMPX_UwbMeas API Structure
========================================


.. rubric:: Global RepCaps

.. code-block:: python
	
	driver = RsCMPX_UwbMeas('TCPIP::192.168.2.101::hislip0')
	# Instance range: Inst1 .. Inst16
	rc = driver.repcap_instance_get()
	driver.repcap_instance_set(repcap.Instance.Inst1)

.. autoclass:: RsCMPX_UwbMeas.RsCMPX_UwbMeas
	:members:
	:undoc-members:
	:noindex:

.. rubric:: Subgroups

.. toctree::
	:maxdepth: 6
	:glob:

	Catalog.rst
	Configure.rst
	Route.rst
	Trigger.rst
	UwbMeas.rst