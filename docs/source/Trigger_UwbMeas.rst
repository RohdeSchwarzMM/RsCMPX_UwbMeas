UwbMeas
----------------------------------------





.. autoclass:: RsCMPX_UwbMeas.Implementations.Trigger.UwbMeas.UwbMeasCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.uwbMeas.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_UwbMeas_MultiEval.rst