MultiEval
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: TRIGger:UWB:MEASurement<Instance>:MEValuation:SOURce
	single: TRIGger:UWB:MEASurement<Instance>:MEValuation:THReshold
	single: TRIGger:UWB:MEASurement<Instance>:MEValuation:TOUT
	single: TRIGger:UWB:MEASurement<Instance>:MEValuation:MGAP
	single: TRIGger:UWB:MEASurement<Instance>:MEValuation:SLOPe
	single: TRIGger:UWB:MEASurement<Instance>:MEValuation:DELay

.. code-block:: python

	TRIGger:UWB:MEASurement<Instance>:MEValuation:SOURce
	TRIGger:UWB:MEASurement<Instance>:MEValuation:THReshold
	TRIGger:UWB:MEASurement<Instance>:MEValuation:TOUT
	TRIGger:UWB:MEASurement<Instance>:MEValuation:MGAP
	TRIGger:UWB:MEASurement<Instance>:MEValuation:SLOPe
	TRIGger:UWB:MEASurement<Instance>:MEValuation:DELay



.. autoclass:: RsCMPX_UwbMeas.Implementations.Trigger.UwbMeas.MultiEval.MultiEvalCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.uwbMeas.multiEval.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_UwbMeas_MultiEval_Catalog.rst