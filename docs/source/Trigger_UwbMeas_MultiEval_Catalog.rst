Catalog
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRIGger:UWB:MEASurement<Instance>:MEValuation:CATalog:SOURce

.. code-block:: python

	TRIGger:UWB:MEASurement<Instance>:MEValuation:CATalog:SOURce



.. autoclass:: RsCMPX_UwbMeas.Implementations.Trigger.UwbMeas.MultiEval.Catalog.CatalogCls
	:members:
	:undoc-members:
	:noindex: