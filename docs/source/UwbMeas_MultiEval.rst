MultiEval
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: INITiate:UWB:MEASurement<Instance>:MEValuation
	single: STOP:UWB:MEASurement<Instance>:MEValuation
	single: ABORt:UWB:MEASurement<Instance>:MEValuation

.. code-block:: python

	INITiate:UWB:MEASurement<Instance>:MEValuation
	STOP:UWB:MEASurement<Instance>:MEValuation
	ABORt:UWB:MEASurement<Instance>:MEValuation



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.MultiEvalCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.uwbMeas.multiEval.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	UwbMeas_MultiEval_Ddecoding.rst
	UwbMeas_MultiEval_Modulation.rst
	UwbMeas_MultiEval_Pmask.rst
	UwbMeas_MultiEval_Power.rst
	UwbMeas_MultiEval_Sinfo.rst
	UwbMeas_MultiEval_State.rst
	UwbMeas_MultiEval_StsSequence.rst
	UwbMeas_MultiEval_Trace.rst
	UwbMeas_MultiEval_TsMask.rst