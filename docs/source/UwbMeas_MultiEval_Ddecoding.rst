Ddecoding
----------------------------------------





.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Ddecoding.DdecodingCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.uwbMeas.multiEval.ddecoding.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	UwbMeas_MultiEval_Ddecoding_Clength.rst
	UwbMeas_MultiEval_Ddecoding_Content.rst
	UwbMeas_MultiEval_Ddecoding_RsParity.rst