Clength
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:DDECoding:CLENgth<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:DDECoding:CLENgth<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:DDECoding:CLENgth<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:DDECoding:CLENgth<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Ddecoding.Clength.ClengthCls
	:members:
	:undoc-members:
	:noindex: