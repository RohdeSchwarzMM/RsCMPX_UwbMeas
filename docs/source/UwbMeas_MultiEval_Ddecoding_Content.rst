Content
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:DDECoding:CONTent<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:DDECoding:CONTent<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:DDECoding:CONTent<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:DDECoding:CONTent<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Ddecoding.Content.ContentCls
	:members:
	:undoc-members:
	:noindex: