RsParity
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:DDECoding:RSParity<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:DDECoding:RSParity<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:DDECoding:RSParity<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:DDECoding:RSParity<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Ddecoding.RsParity.RsParityCls
	:members:
	:undoc-members:
	:noindex: