Modulation
----------------------------------------





.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Modulation.ModulationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.uwbMeas.multiEval.modulation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	UwbMeas_MultiEval_Modulation_Average.rst
	UwbMeas_MultiEval_Modulation_CcError.rst
	UwbMeas_MultiEval_Modulation_Cevm.rst
	UwbMeas_MultiEval_Modulation_CpJitter.rst
	UwbMeas_MultiEval_Modulation_CtJitter.rst
	UwbMeas_MultiEval_Modulation_Current.rst
	UwbMeas_MultiEval_Modulation_Extreme.rst
	UwbMeas_MultiEval_Modulation_Foffset.rst
	UwbMeas_MultiEval_Modulation_Fofh.rst
	UwbMeas_MultiEval_Modulation_IqOffset.rst
	UwbMeas_MultiEval_Modulation_Nmse.rst
	UwbMeas_MultiEval_Modulation_Otolerance.rst
	UwbMeas_MultiEval_Modulation_Phr.rst
	UwbMeas_MultiEval_Modulation_Plevel.rst
	UwbMeas_MultiEval_Modulation_PmlWidth.rst
	UwbMeas_MultiEval_Modulation_Psdu.rst
	UwbMeas_MultiEval_Modulation_Rmarker.rst
	UwbMeas_MultiEval_Modulation_Sevm.rst
	UwbMeas_MultiEval_Modulation_Sfd.rst
	UwbMeas_MultiEval_Modulation_Shr.rst
	UwbMeas_MultiEval_Modulation_SlPeak.rst
	UwbMeas_MultiEval_Modulation_SmAccuracy.rst
	UwbMeas_MultiEval_Modulation_SpJitter.rst
	UwbMeas_MultiEval_Modulation_StandardDev.rst
	UwbMeas_MultiEval_Modulation_StJitter.rst
	UwbMeas_MultiEval_Modulation_Sts.rst
	UwbMeas_MultiEval_Modulation_Sync.rst