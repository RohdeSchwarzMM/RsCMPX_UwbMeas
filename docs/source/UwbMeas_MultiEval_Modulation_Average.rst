Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:AVERage<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:MODulation:AVERage<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:AVERage<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:MODulation:AVERage<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Modulation.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: