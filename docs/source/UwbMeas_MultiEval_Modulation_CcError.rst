CcError
----------------------------------------





.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Modulation.CcError.CcErrorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.uwbMeas.multiEval.modulation.ccError.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	UwbMeas_MultiEval_Modulation_CcError_Average.rst
	UwbMeas_MultiEval_Modulation_CcError_Current.rst
	UwbMeas_MultiEval_Modulation_CcError_Extreme.rst
	UwbMeas_MultiEval_Modulation_CcError_StandardDev.rst