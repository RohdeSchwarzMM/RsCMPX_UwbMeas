Current<Ppdu>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr100
	rc = driver.uwbMeas.multiEval.modulation.ccError.current.repcap_ppdu_get()
	driver.uwbMeas.multiEval.modulation.ccError.current.repcap_ppdu_set(repcap.Ppdu.Nr1)



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:CCERor:CURRent<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:MODulation:CCERor:CURRent<PPDU>
	single: CALCulate:UWB:MEASurement<Instance>:MEValuation:MODulation:CCERor:CURRent<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:CCERor:CURRent<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:MODulation:CCERor:CURRent<PPDU>
	CALCulate:UWB:MEASurement<Instance>:MEValuation:MODulation:CCERor:CURRent<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Modulation.CcError.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.uwbMeas.multiEval.modulation.ccError.current.clone()