Extreme
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:CCERor:EXTReme<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:MODulation:CCERor:EXTReme<PPDU>
	single: CALCulate:UWB:MEASurement<Instance>:MEValuation:MODulation:CCERor:EXTReme<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:CCERor:EXTReme<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:MODulation:CCERor:EXTReme<PPDU>
	CALCulate:UWB:MEASurement<Instance>:MEValuation:MODulation:CCERor:EXTReme<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Modulation.CcError.Extreme.ExtremeCls
	:members:
	:undoc-members:
	:noindex: