Cevm
----------------------------------------





.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Modulation.Cevm.CevmCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.uwbMeas.multiEval.modulation.cevm.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	UwbMeas_MultiEval_Modulation_Cevm_Average.rst
	UwbMeas_MultiEval_Modulation_Cevm_Current.rst
	UwbMeas_MultiEval_Modulation_Cevm_Extreme.rst
	UwbMeas_MultiEval_Modulation_Cevm_StandardDev.rst