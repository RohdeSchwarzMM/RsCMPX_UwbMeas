Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:CEVM:AVERage<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:MODulation:CEVM:AVERage<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:CEVM:AVERage<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:MODulation:CEVM:AVERage<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Modulation.Cevm.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: