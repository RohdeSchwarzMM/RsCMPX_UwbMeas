StandardDev
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:CEVM:SDEViation<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:MODulation:CEVM:SDEViation<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:CEVM:SDEViation<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:MODulation:CEVM:SDEViation<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Modulation.Cevm.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: