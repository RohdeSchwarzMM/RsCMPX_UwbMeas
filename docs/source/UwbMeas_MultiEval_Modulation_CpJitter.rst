CpJitter
----------------------------------------





.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Modulation.CpJitter.CpJitterCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.uwbMeas.multiEval.modulation.cpJitter.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	UwbMeas_MultiEval_Modulation_CpJitter_Average.rst
	UwbMeas_MultiEval_Modulation_CpJitter_Current.rst
	UwbMeas_MultiEval_Modulation_CpJitter_Extreme.rst
	UwbMeas_MultiEval_Modulation_CpJitter_StandardDev.rst