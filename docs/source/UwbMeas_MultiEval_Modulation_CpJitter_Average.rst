Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:CPJitter:AVERage<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:MODulation:CPJitter:AVERage<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:CPJitter:AVERage<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:MODulation:CPJitter:AVERage<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Modulation.CpJitter.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: