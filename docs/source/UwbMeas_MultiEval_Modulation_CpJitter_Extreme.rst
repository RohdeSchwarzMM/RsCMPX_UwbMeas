Extreme
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:CPJitter:EXTReme<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:MODulation:CPJitter:EXTReme<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:CPJitter:EXTReme<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:MODulation:CPJitter:EXTReme<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Modulation.CpJitter.Extreme.ExtremeCls
	:members:
	:undoc-members:
	:noindex: