CtJitter
----------------------------------------





.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Modulation.CtJitter.CtJitterCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.uwbMeas.multiEval.modulation.ctJitter.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	UwbMeas_MultiEval_Modulation_CtJitter_Average.rst
	UwbMeas_MultiEval_Modulation_CtJitter_Current.rst
	UwbMeas_MultiEval_Modulation_CtJitter_Extreme.rst
	UwbMeas_MultiEval_Modulation_CtJitter_StandardDev.rst