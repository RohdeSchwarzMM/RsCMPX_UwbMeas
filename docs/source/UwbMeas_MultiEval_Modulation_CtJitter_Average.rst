Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:CTJittter:AVERage<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:MODulation:CTJittter:AVERage<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:CTJittter:AVERage<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:MODulation:CTJittter:AVERage<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Modulation.CtJitter.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: