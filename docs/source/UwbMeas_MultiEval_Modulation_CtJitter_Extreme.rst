Extreme
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:CTJittter:EXTReme<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:MODulation:CTJittter:EXTReme<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:CTJittter:EXTReme<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:MODulation:CTJittter:EXTReme<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Modulation.CtJitter.Extreme.ExtremeCls
	:members:
	:undoc-members:
	:noindex: