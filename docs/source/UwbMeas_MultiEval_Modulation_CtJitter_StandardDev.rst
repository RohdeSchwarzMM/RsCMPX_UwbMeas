StandardDev
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:CTJittter:SDEViation<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:MODulation:CTJittter:SDEViation<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:CTJittter:SDEViation<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:MODulation:CTJittter:SDEViation<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Modulation.CtJitter.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: