Extreme
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:EXTReme<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:MODulation:EXTReme<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:EXTReme<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:MODulation:EXTReme<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Modulation.Extreme.ExtremeCls
	:members:
	:undoc-members:
	:noindex: