Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:FOFFset:AVERage<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:MODulation:FOFFset:AVERage<PPDU>
	single: CALCulate:UWB:MEASurement<Instance>:MEValuation:MODulation:FOFFset:AVERage<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:FOFFset:AVERage<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:MODulation:FOFFset:AVERage<PPDU>
	CALCulate:UWB:MEASurement<Instance>:MEValuation:MODulation:FOFFset:AVERage<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Modulation.Foffset.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: