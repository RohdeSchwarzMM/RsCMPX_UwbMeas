Extreme
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:FOFFset:EXTReme<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:MODulation:FOFFset:EXTReme<PPDU>
	single: CALCulate:UWB:MEASurement<Instance>:MEValuation:MODulation:FOFFset:EXTReme<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:FOFFset:EXTReme<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:MODulation:FOFFset:EXTReme<PPDU>
	CALCulate:UWB:MEASurement<Instance>:MEValuation:MODulation:FOFFset:EXTReme<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Modulation.Foffset.Extreme.ExtremeCls
	:members:
	:undoc-members:
	:noindex: