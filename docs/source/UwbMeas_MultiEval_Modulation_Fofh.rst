Fofh
----------------------------------------





.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Modulation.Fofh.FofhCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.uwbMeas.multiEval.modulation.fofh.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	UwbMeas_MultiEval_Modulation_Fofh_Average.rst
	UwbMeas_MultiEval_Modulation_Fofh_Current.rst
	UwbMeas_MultiEval_Modulation_Fofh_Extreme.rst
	UwbMeas_MultiEval_Modulation_Fofh_StandardDev.rst