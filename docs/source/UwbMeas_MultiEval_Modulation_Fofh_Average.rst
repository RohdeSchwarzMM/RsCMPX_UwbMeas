Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:FOFH:AVERage<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:MODulation:FOFH:AVERage<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:FOFH:AVERage<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:MODulation:FOFH:AVERage<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Modulation.Fofh.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: