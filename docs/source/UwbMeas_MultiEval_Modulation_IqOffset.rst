IqOffset
----------------------------------------





.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Modulation.IqOffset.IqOffsetCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.uwbMeas.multiEval.modulation.iqOffset.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	UwbMeas_MultiEval_Modulation_IqOffset_Average.rst
	UwbMeas_MultiEval_Modulation_IqOffset_Current.rst
	UwbMeas_MultiEval_Modulation_IqOffset_Extreme.rst
	UwbMeas_MultiEval_Modulation_IqOffset_StandardDev.rst