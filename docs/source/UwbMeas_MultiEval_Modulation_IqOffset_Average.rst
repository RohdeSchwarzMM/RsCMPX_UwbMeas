Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:IQOFfset:AVERage<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:MODulation:IQOFfset:AVERage<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:IQOFfset:AVERage<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:MODulation:IQOFfset:AVERage<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Modulation.IqOffset.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: