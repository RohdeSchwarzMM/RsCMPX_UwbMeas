Extreme
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:IQOFfset:EXTReme<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:MODulation:IQOFfset:EXTReme<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:IQOFfset:EXTReme<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:MODulation:IQOFfset:EXTReme<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Modulation.IqOffset.Extreme.ExtremeCls
	:members:
	:undoc-members:
	:noindex: