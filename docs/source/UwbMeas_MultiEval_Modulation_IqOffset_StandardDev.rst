StandardDev
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:IQOFfset:SDEViation<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:MODulation:IQOFfset:SDEViation<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:IQOFfset:SDEViation<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:MODulation:IQOFfset:SDEViation<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Modulation.IqOffset.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: