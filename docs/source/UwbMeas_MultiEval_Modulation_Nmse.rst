Nmse
----------------------------------------





.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Modulation.Nmse.NmseCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.uwbMeas.multiEval.modulation.nmse.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	UwbMeas_MultiEval_Modulation_Nmse_Average.rst
	UwbMeas_MultiEval_Modulation_Nmse_Current.rst
	UwbMeas_MultiEval_Modulation_Nmse_Extreme.rst
	UwbMeas_MultiEval_Modulation_Nmse_StandardDev.rst