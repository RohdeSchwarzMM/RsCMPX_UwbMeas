Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:NMSE:AVERage<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:MODulation:NMSE:AVERage<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:NMSE:AVERage<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:MODulation:NMSE:AVERage<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Modulation.Nmse.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: