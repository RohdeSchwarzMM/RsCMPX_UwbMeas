Otolerance
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:UWB:MEASurement<Instance>:MEValuation:MODulation:OTOLerance
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:OTOLerance

.. code-block:: python

	READ:UWB:MEASurement<Instance>:MEValuation:MODulation:OTOLerance
	FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:OTOLerance



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Modulation.Otolerance.OtoleranceCls
	:members:
	:undoc-members:
	:noindex: