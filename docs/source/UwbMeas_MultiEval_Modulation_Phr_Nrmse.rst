Nrmse
----------------------------------------





.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Modulation.Phr.Nrmse.NrmseCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.uwbMeas.multiEval.modulation.phr.nrmse.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	UwbMeas_MultiEval_Modulation_Phr_Nrmse_Average.rst
	UwbMeas_MultiEval_Modulation_Phr_Nrmse_Current.rst
	UwbMeas_MultiEval_Modulation_Phr_Nrmse_Extreme.rst
	UwbMeas_MultiEval_Modulation_Phr_Nrmse_StandardDev.rst