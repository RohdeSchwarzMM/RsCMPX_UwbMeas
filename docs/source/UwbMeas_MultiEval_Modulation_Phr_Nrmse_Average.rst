Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:PHR:NRMSe:AVERage<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:MODulation:PHR:NRMSe:AVERage<PPDU>
	single: CALCulate:UWB:MEASurement<Instance>:MEValuation:MODulation:PHR:NRMSe:AVERage<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:PHR:NRMSe:AVERage<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:MODulation:PHR:NRMSe:AVERage<PPDU>
	CALCulate:UWB:MEASurement<Instance>:MEValuation:MODulation:PHR:NRMSe:AVERage<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Modulation.Phr.Nrmse.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: