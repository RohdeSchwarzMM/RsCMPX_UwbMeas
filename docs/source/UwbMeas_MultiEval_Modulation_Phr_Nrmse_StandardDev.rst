StandardDev
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:PHR:NRMSe:SDEViation<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:MODulation:PHR:NRMSe:SDEViation<PPDU>
	single: CALCulate:UWB:MEASurement<Instance>:MEValuation:MODulation:PHR:NRMSe:SDEViation<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:PHR:NRMSe:SDEViation<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:MODulation:PHR:NRMSe:SDEViation<PPDU>
	CALCulate:UWB:MEASurement<Instance>:MEValuation:MODulation:PHR:NRMSe:SDEViation<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Modulation.Phr.Nrmse.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: