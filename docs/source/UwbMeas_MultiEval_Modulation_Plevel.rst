Plevel
----------------------------------------





.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Modulation.Plevel.PlevelCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.uwbMeas.multiEval.modulation.plevel.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	UwbMeas_MultiEval_Modulation_Plevel_Average.rst
	UwbMeas_MultiEval_Modulation_Plevel_Current.rst
	UwbMeas_MultiEval_Modulation_Plevel_Maximum.rst
	UwbMeas_MultiEval_Modulation_Plevel_Minimum.rst
	UwbMeas_MultiEval_Modulation_Plevel_StandardDev.rst