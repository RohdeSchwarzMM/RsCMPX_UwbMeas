Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:PLEVel:AVERage<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:MODulation:PLEVel:AVERage<PPDU>
	single: CALCulate:UWB:MEASurement<Instance>:MEValuation:MODulation:PLEVel:AVERage<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:PLEVel:AVERage<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:MODulation:PLEVel:AVERage<PPDU>
	CALCulate:UWB:MEASurement<Instance>:MEValuation:MODulation:PLEVel:AVERage<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Modulation.Plevel.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: