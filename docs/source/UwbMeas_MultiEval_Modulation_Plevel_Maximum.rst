Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:PLEVel:MAXimum<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:MODulation:PLEVel:MAXimum<PPDU>
	single: CALCulate:UWB:MEASurement<Instance>:MEValuation:MODulation:PLEVel:MAXimum<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:PLEVel:MAXimum<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:MODulation:PLEVel:MAXimum<PPDU>
	CALCulate:UWB:MEASurement<Instance>:MEValuation:MODulation:PLEVel:MAXimum<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Modulation.Plevel.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: