PmlWidth
----------------------------------------





.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Modulation.PmlWidth.PmlWidthCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.uwbMeas.multiEval.modulation.pmlWidth.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	UwbMeas_MultiEval_Modulation_PmlWidth_Average.rst
	UwbMeas_MultiEval_Modulation_PmlWidth_Current.rst
	UwbMeas_MultiEval_Modulation_PmlWidth_Extreme.rst
	UwbMeas_MultiEval_Modulation_PmlWidth_StandardDev.rst