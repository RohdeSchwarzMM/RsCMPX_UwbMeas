Extreme
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:PMLWidth:EXTReme<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:MODulation:PMLWidth:EXTReme<PPDU>
	single: CALCulate:UWB:MEASurement<Instance>:MEValuation:MODulation:PMLWidth:EXTReme<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:PMLWidth:EXTReme<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:MODulation:PMLWidth:EXTReme<PPDU>
	CALCulate:UWB:MEASurement<Instance>:MEValuation:MODulation:PMLWidth:EXTReme<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Modulation.PmlWidth.Extreme.ExtremeCls
	:members:
	:undoc-members:
	:noindex: