Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:PSDU:NRMSe:AVERage<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:MODulation:PSDU:NRMSe:AVERage<PPDU>
	single: CALCulate:UWB:MEASurement<Instance>:MEValuation:MODulation:PSDU:NRMSe:AVERage<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:PSDU:NRMSe:AVERage<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:MODulation:PSDU:NRMSe:AVERage<PPDU>
	CALCulate:UWB:MEASurement<Instance>:MEValuation:MODulation:PSDU:NRMSe:AVERage<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Modulation.Psdu.Nrmse.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: