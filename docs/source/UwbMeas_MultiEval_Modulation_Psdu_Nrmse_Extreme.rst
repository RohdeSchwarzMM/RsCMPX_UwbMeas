Extreme
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:PSDU:NRMSe:EXTReme<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:MODulation:PSDU:NRMSe:EXTReme<PPDU>
	single: CALCulate:UWB:MEASurement<Instance>:MEValuation:MODulation:PSDU:NRMSe:EXTReme<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:PSDU:NRMSe:EXTReme<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:MODulation:PSDU:NRMSe:EXTReme<PPDU>
	CALCulate:UWB:MEASurement<Instance>:MEValuation:MODulation:PSDU:NRMSe:EXTReme<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Modulation.Psdu.Nrmse.Extreme.ExtremeCls
	:members:
	:undoc-members:
	:noindex: