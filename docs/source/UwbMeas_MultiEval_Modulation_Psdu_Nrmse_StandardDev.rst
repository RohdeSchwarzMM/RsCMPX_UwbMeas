StandardDev
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:PSDU:NRMSe:SDEViation<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:MODulation:PSDU:NRMSe:SDEViation<PPDU>
	single: CALCulate:UWB:MEASurement<Instance>:MEValuation:MODulation:PSDU:NRMSe:SDEViation<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:PSDU:NRMSe:SDEViation<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:MODulation:PSDU:NRMSe:SDEViation<PPDU>
	CALCulate:UWB:MEASurement<Instance>:MEValuation:MODulation:PSDU:NRMSe:SDEViation<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Modulation.Psdu.Nrmse.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: