Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:PSDU:PLEVel:AVERage<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:MODulation:PSDU:PLEVel:AVERage<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:PSDU:PLEVel:AVERage<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:MODulation:PSDU:PLEVel:AVERage<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Modulation.Psdu.Plevel.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: