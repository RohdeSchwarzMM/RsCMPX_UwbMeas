Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:PSDU:PLEVel:MAXimum<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:MODulation:PSDU:PLEVel:MAXimum<PPDU>
	single: CALCulate:UWB:MEASurement<Instance>:MEValuation:MODulation:PSDU:PLEVel:MAXimum<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:PSDU:PLEVel:MAXimum<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:MODulation:PSDU:PLEVel:MAXimum<PPDU>
	CALCulate:UWB:MEASurement<Instance>:MEValuation:MODulation:PSDU:PLEVel:MAXimum<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Modulation.Psdu.Plevel.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: