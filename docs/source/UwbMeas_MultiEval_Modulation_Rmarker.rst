Rmarker
----------------------------------------





.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Modulation.Rmarker.RmarkerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.uwbMeas.multiEval.modulation.rmarker.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	UwbMeas_MultiEval_Modulation_Rmarker_Average.rst
	UwbMeas_MultiEval_Modulation_Rmarker_Current.rst
	UwbMeas_MultiEval_Modulation_Rmarker_Extreme.rst
	UwbMeas_MultiEval_Modulation_Rmarker_StandardDev.rst