Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:RMARker:AVERage<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:MODulation:RMARker:AVERage<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:RMARker:AVERage<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:MODulation:RMARker:AVERage<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Modulation.Rmarker.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: