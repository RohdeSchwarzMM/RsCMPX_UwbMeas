StandardDev
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:RMARker:SDEViation<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:MODulation:RMARker:SDEViation<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:RMARker:SDEViation<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:MODulation:RMARker:SDEViation<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Modulation.Rmarker.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: