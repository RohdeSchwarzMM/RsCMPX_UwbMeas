Sevm
----------------------------------------





.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Modulation.Sevm.SevmCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.uwbMeas.multiEval.modulation.sevm.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	UwbMeas_MultiEval_Modulation_Sevm_Average.rst
	UwbMeas_MultiEval_Modulation_Sevm_Current.rst
	UwbMeas_MultiEval_Modulation_Sevm_Extreme.rst
	UwbMeas_MultiEval_Modulation_Sevm_StandardDev.rst