Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:SEVM:AVERage<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:MODulation:SEVM:AVERage<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:SEVM:AVERage<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:MODulation:SEVM:AVERage<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Modulation.Sevm.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: