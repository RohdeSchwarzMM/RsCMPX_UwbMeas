Extreme
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:SFD:PLPolarity:EXTReme<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:MODulation:SFD:PLPolarity:EXTReme<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:SFD:PLPolarity:EXTReme<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:MODulation:SFD:PLPolarity:EXTReme<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Modulation.Sfd.PlPolarity.Extreme.ExtremeCls
	:members:
	:undoc-members:
	:noindex: