SlPeak
----------------------------------------





.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Modulation.SlPeak.SlPeakCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.uwbMeas.multiEval.modulation.slPeak.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	UwbMeas_MultiEval_Modulation_SlPeak_Average.rst
	UwbMeas_MultiEval_Modulation_SlPeak_Current.rst
	UwbMeas_MultiEval_Modulation_SlPeak_Extreme.rst
	UwbMeas_MultiEval_Modulation_SlPeak_StandardDev.rst