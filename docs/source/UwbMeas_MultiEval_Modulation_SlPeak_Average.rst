Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:SLPeak:AVERage<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:MODulation:SLPeak:AVERage<PPDU>
	single: CALCulate:UWB:MEASurement<Instance>:MEValuation:MODulation:SLPeak:AVERage<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:SLPeak:AVERage<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:MODulation:SLPeak:AVERage<PPDU>
	CALCulate:UWB:MEASurement<Instance>:MEValuation:MODulation:SLPeak:AVERage<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Modulation.SlPeak.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: