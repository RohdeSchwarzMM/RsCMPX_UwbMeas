Extreme
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:SLPeak:EXTReme<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:MODulation:SLPeak:EXTReme<PPDU>
	single: CALCulate:UWB:MEASurement<Instance>:MEValuation:MODulation:SLPeak:EXTReme<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:SLPeak:EXTReme<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:MODulation:SLPeak:EXTReme<PPDU>
	CALCulate:UWB:MEASurement<Instance>:MEValuation:MODulation:SLPeak:EXTReme<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Modulation.SlPeak.Extreme.ExtremeCls
	:members:
	:undoc-members:
	:noindex: