SmAccuracy
----------------------------------------





.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Modulation.SmAccuracy.SmAccuracyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.uwbMeas.multiEval.modulation.smAccuracy.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	UwbMeas_MultiEval_Modulation_SmAccuracy_Average.rst
	UwbMeas_MultiEval_Modulation_SmAccuracy_Current.rst
	UwbMeas_MultiEval_Modulation_SmAccuracy_Extreme.rst
	UwbMeas_MultiEval_Modulation_SmAccuracy_StandardDev.rst