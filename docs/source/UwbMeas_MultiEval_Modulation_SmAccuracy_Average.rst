Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:SMACcuracy:AVERage<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:MODulation:SMACcuracy:AVERage<PPDU>
	single: CALCulate:UWB:MEASurement<Instance>:MEValuation:MODulation:SMACcuracy:AVERage<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:SMACcuracy:AVERage<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:MODulation:SMACcuracy:AVERage<PPDU>
	CALCulate:UWB:MEASurement<Instance>:MEValuation:MODulation:SMACcuracy:AVERage<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Modulation.SmAccuracy.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: