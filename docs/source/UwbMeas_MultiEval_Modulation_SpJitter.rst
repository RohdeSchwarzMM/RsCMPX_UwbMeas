SpJitter
----------------------------------------





.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Modulation.SpJitter.SpJitterCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.uwbMeas.multiEval.modulation.spJitter.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	UwbMeas_MultiEval_Modulation_SpJitter_Average.rst
	UwbMeas_MultiEval_Modulation_SpJitter_Current.rst
	UwbMeas_MultiEval_Modulation_SpJitter_Extreme.rst
	UwbMeas_MultiEval_Modulation_SpJitter_StandardDev.rst