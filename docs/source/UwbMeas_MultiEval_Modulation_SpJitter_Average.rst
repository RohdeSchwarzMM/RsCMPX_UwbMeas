Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:SPJitter:AVERage<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:MODulation:SPJitter:AVERage<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:SPJitter:AVERage<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:MODulation:SPJitter:AVERage<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Modulation.SpJitter.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: