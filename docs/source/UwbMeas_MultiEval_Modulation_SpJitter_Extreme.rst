Extreme
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:SPJitter:EXTReme<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:MODulation:SPJitter:EXTReme<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:SPJitter:EXTReme<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:MODulation:SPJitter:EXTReme<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Modulation.SpJitter.Extreme.ExtremeCls
	:members:
	:undoc-members:
	:noindex: