StandardDev
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:SPJitter:SDEViation<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:MODulation:SPJitter:SDEViation<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:SPJitter:SDEViation<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:MODulation:SPJitter:SDEViation<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Modulation.SpJitter.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: