StJitter
----------------------------------------





.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Modulation.StJitter.StJitterCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.uwbMeas.multiEval.modulation.stJitter.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	UwbMeas_MultiEval_Modulation_StJitter_Average.rst
	UwbMeas_MultiEval_Modulation_StJitter_Current.rst
	UwbMeas_MultiEval_Modulation_StJitter_Extreme.rst
	UwbMeas_MultiEval_Modulation_StJitter_StandardDev.rst