Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:STJitter:AVERage<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:MODulation:STJitter:AVERage<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:STJitter:AVERage<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:MODulation:STJitter:AVERage<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Modulation.StJitter.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: