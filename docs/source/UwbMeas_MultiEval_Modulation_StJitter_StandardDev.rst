StandardDev
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:STJitter:SDEViation<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:MODulation:STJitter:SDEViation<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:STJitter:SDEViation<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:MODulation:STJitter:SDEViation<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Modulation.StJitter.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: