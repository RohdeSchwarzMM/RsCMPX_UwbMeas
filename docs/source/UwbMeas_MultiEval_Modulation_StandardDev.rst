StandardDev
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:SDEViation<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:MODulation:SDEViation<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:SDEViation<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:MODulation:SDEViation<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Modulation.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: