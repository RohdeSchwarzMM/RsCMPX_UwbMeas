Sts
----------------------------------------





.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Modulation.Sts.StsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.uwbMeas.multiEval.modulation.sts.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	UwbMeas_MultiEval_Modulation_Sts_Nrmse.rst
	UwbMeas_MultiEval_Modulation_Sts_Plevel.rst
	UwbMeas_MultiEval_Modulation_Sts_PlPolarity.rst