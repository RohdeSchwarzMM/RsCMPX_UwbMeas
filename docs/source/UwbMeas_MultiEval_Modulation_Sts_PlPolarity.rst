PlPolarity
----------------------------------------





.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Modulation.Sts.PlPolarity.PlPolarityCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.uwbMeas.multiEval.modulation.sts.plPolarity.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	UwbMeas_MultiEval_Modulation_Sts_PlPolarity_Current.rst
	UwbMeas_MultiEval_Modulation_Sts_PlPolarity_Extreme.rst