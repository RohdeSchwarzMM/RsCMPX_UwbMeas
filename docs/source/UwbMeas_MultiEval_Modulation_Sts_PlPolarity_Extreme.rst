Extreme
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:STS:PLPolarity:EXTReme<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:MODulation:STS:PLPolarity:EXTReme<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:STS:PLPolarity:EXTReme<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:MODulation:STS:PLPolarity:EXTReme<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Modulation.Sts.PlPolarity.Extreme.ExtremeCls
	:members:
	:undoc-members:
	:noindex: