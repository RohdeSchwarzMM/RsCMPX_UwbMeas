Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:STS:PLEVel:AVERage<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:MODulation:STS:PLEVel:AVERage<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:STS:PLEVel:AVERage<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:MODulation:STS:PLEVel:AVERage<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Modulation.Sts.Plevel.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: