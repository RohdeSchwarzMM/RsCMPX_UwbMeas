Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:STS:PLEVel:MAXimum<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:MODulation:STS:PLEVel:MAXimum<PPDU>
	single: CALCulate:UWB:MEASurement<Instance>:MEValuation:MODulation:STS:PLEVel:MAXimum<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:STS:PLEVel:MAXimum<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:MODulation:STS:PLEVel:MAXimum<PPDU>
	CALCulate:UWB:MEASurement<Instance>:MEValuation:MODulation:STS:PLEVel:MAXimum<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Modulation.Sts.Plevel.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: