Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:STS:PLEVel:MINimum<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:MODulation:STS:PLEVel:MINimum<PPDU>
	single: CALCulate:UWB:MEASurement<Instance>:MEValuation:MODulation:STS:PLEVel:MINimum<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:STS:PLEVel:MINimum<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:MODulation:STS:PLEVel:MINimum<PPDU>
	CALCulate:UWB:MEASurement<Instance>:MEValuation:MODulation:STS:PLEVel:MINimum<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Modulation.Sts.Plevel.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: