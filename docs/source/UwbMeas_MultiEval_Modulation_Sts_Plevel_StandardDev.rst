StandardDev
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:STS:PLEVel:SDEViation<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:MODulation:STS:PLEVel:SDEViation<PPDU>
	single: CALCulate:UWB:MEASurement<Instance>:MEValuation:MODulation:STS:PLEVel:SDEViation<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:MODulation:STS:PLEVel:SDEViation<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:MODulation:STS:PLEVel:SDEViation<PPDU>
	CALCulate:UWB:MEASurement<Instance>:MEValuation:MODulation:STS:PLEVel:SDEViation<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Modulation.Sts.Plevel.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: