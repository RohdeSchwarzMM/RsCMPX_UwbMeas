Pmask
----------------------------------------





.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Pmask.PmaskCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.uwbMeas.multiEval.pmask.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	UwbMeas_MultiEval_Pmask_Lower.rst
	UwbMeas_MultiEval_Pmask_Margin.rst
	UwbMeas_MultiEval_Pmask_Otolerance.rst
	UwbMeas_MultiEval_Pmask_Upper.rst