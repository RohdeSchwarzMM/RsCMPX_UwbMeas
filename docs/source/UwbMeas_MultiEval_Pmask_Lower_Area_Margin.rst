Margin
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:PMASk:LOWer:AREA<nr>:MARGin<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:PMASk:LOWer:AREA<nr>:MARGin<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:PMASk:LOWer:AREA<nr>:MARGin<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:PMASk:LOWer:AREA<nr>:MARGin<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Pmask.Lower.Area.Margin.MarginCls
	:members:
	:undoc-members:
	:noindex: