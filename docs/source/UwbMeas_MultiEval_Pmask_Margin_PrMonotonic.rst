PrMonotonic
----------------------------------------





.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Pmask.Margin.PrMonotonic.PrMonotonicCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.uwbMeas.multiEval.pmask.margin.prMonotonic.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	UwbMeas_MultiEval_Pmask_Margin_PrMonotonic_Current.rst
	UwbMeas_MultiEval_Pmask_Margin_PrMonotonic_Extreme.rst