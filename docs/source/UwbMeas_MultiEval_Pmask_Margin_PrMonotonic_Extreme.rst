Extreme
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:PMASk:MARGin:PRMonotonic:EXTReme<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:PMASk:MARGin:PRMonotonic:EXTReme<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:PMASk:MARGin:PRMonotonic:EXTReme<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:PMASk:MARGin:PRMonotonic:EXTReme<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Pmask.Margin.PrMonotonic.Extreme.ExtremeCls
	:members:
	:undoc-members:
	:noindex: