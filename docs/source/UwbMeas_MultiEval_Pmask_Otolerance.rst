Otolerance
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:PMASk:OTOLerance
	single: READ:UWB:MEASurement<Instance>:MEValuation:PMASk:OTOLerance

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:PMASk:OTOLerance
	READ:UWB:MEASurement<Instance>:MEValuation:PMASk:OTOLerance



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Pmask.Otolerance.OtoleranceCls
	:members:
	:undoc-members:
	:noindex: