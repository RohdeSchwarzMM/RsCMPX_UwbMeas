Power
----------------------------------------





.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Power.PowerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.uwbMeas.multiEval.power.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	UwbMeas_MultiEval_Power_Average.rst
	UwbMeas_MultiEval_Power_Current.rst
	UwbMeas_MultiEval_Power_Dpower.rst
	UwbMeas_MultiEval_Power_DpPower.rst
	UwbMeas_MultiEval_Power_Maximum.rst
	UwbMeas_MultiEval_Power_Minimum.rst
	UwbMeas_MultiEval_Power_MsfPower.rst
	UwbMeas_MultiEval_Power_MsPower.rst
	UwbMeas_MultiEval_Power_PpdPeak.rst
	UwbMeas_MultiEval_Power_Ppdu.rst
	UwbMeas_MultiEval_Power_Ppower.rst
	UwbMeas_MultiEval_Power_PpPower.rst
	UwbMeas_MultiEval_Power_StandardDev.rst