Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:POWer:AVERage<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:POWer:AVERage<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:POWer:AVERage<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:POWer:AVERage<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Power.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: