DpPower
----------------------------------------





.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Power.DpPower.DpPowerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.uwbMeas.multiEval.power.dpPower.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	UwbMeas_MultiEval_Power_DpPower_Average.rst
	UwbMeas_MultiEval_Power_DpPower_Current.rst
	UwbMeas_MultiEval_Power_DpPower_Maximum.rst
	UwbMeas_MultiEval_Power_DpPower_Minimum.rst
	UwbMeas_MultiEval_Power_DpPower_StandardDev.rst