Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:POWer:DPPower:AVERage<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:POWer:DPPower:AVERage<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:POWer:DPPower:AVERage<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:POWer:DPPower:AVERage<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Power.DpPower.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: