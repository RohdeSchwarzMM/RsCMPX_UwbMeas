Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:POWer:DPPower:MAXimum<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:POWer:DPPower:MAXimum<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:POWer:DPPower:MAXimum<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:POWer:DPPower:MAXimum<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Power.DpPower.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: