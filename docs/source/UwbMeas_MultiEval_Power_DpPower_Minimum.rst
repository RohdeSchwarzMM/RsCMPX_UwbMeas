Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:POWer:DPPower:MINimum<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:POWer:DPPower:MINimum<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:POWer:DPPower:MINimum<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:POWer:DPPower:MINimum<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Power.DpPower.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: