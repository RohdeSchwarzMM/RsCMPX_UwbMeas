StandardDev
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:POWer:DPPower:SDEViation<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:POWer:DPPower:SDEViation<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:POWer:DPPower:SDEViation<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:POWer:DPPower:SDEViation<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Power.DpPower.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: