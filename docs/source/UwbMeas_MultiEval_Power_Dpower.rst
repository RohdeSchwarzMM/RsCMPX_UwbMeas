Dpower
----------------------------------------





.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Power.Dpower.DpowerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.uwbMeas.multiEval.power.dpower.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	UwbMeas_MultiEval_Power_Dpower_Average.rst
	UwbMeas_MultiEval_Power_Dpower_Current.rst
	UwbMeas_MultiEval_Power_Dpower_Maximum.rst
	UwbMeas_MultiEval_Power_Dpower_Minimum.rst
	UwbMeas_MultiEval_Power_Dpower_StandardDev.rst