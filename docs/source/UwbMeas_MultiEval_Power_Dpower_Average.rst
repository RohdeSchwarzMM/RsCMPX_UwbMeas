Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:POWer:DPOWer:AVERage<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:POWer:DPOWer:AVERage<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:POWer:DPOWer:AVERage<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:POWer:DPOWer:AVERage<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Power.Dpower.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: