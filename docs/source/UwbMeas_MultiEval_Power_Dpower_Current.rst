Current<Ppdu>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr100
	rc = driver.uwbMeas.multiEval.power.dpower.current.repcap_ppdu_get()
	driver.uwbMeas.multiEval.power.dpower.current.repcap_ppdu_set(repcap.Ppdu.Nr1)



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:POWer:DPOWer:CURRent<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:POWer:DPOWer:CURRent<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:POWer:DPOWer:CURRent<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:POWer:DPOWer:CURRent<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Power.Dpower.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.uwbMeas.multiEval.power.dpower.current.clone()