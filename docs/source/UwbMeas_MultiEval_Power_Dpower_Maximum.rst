Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:POWer:DPOWer:MAXimum<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:POWer:DPOWer:MAXimum<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:POWer:DPOWer:MAXimum<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:POWer:DPOWer:MAXimum<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Power.Dpower.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: