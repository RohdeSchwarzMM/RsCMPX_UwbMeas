Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:POWer:DPOWer:MINimum<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:POWer:DPOWer:MINimum<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:POWer:DPOWer:MINimum<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:POWer:DPOWer:MINimum<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Power.Dpower.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: