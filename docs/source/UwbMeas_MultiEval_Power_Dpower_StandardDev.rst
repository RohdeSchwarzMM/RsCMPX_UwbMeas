StandardDev
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:POWer:DPOWer:SDEViation<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:POWer:DPOWer:SDEViation<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:POWer:DPOWer:SDEViation<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:POWer:DPOWer:SDEViation<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Power.Dpower.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: