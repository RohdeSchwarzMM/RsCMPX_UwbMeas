Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:POWer:MAXimum<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:POWer:MAXimum<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:POWer:MAXimum<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:POWer:MAXimum<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Power.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: