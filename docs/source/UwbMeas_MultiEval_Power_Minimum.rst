Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:POWer:MINimum<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:POWer:MINimum<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:POWer:MINimum<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:POWer:MINimum<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Power.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: