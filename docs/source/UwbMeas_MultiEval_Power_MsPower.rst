MsPower
----------------------------------------





.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Power.MsPower.MsPowerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.uwbMeas.multiEval.power.msPower.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	UwbMeas_MultiEval_Power_MsPower_Average.rst
	UwbMeas_MultiEval_Power_MsPower_Current.rst
	UwbMeas_MultiEval_Power_MsPower_Maximum.rst
	UwbMeas_MultiEval_Power_MsPower_Minimum.rst
	UwbMeas_MultiEval_Power_MsPower_StandardDev.rst