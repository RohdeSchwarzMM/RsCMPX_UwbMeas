Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:POWer:MSPower:AVERage<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:POWer:MSPower:AVERage<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:POWer:MSPower:AVERage<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:POWer:MSPower:AVERage<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Power.MsPower.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: