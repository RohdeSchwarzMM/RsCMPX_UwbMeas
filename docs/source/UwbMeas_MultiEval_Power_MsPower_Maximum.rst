Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:POWer:MSPower:MAXimum<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:POWer:MSPower:MAXimum<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:POWer:MSPower:MAXimum<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:POWer:MSPower:MAXimum<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Power.MsPower.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: