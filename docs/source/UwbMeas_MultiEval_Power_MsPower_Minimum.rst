Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:POWer:MSPower:MINimum<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:POWer:MSPower:MINimum<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:POWer:MSPower:MINimum<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:POWer:MSPower:MINimum<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Power.MsPower.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: