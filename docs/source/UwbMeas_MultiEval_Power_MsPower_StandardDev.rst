StandardDev
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:POWer:MSPower:SDEViation<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:POWer:MSPower:SDEViation<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:POWer:MSPower:SDEViation<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:POWer:MSPower:SDEViation<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Power.MsPower.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: