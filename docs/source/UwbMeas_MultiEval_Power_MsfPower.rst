MsfPower
----------------------------------------





.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Power.MsfPower.MsfPowerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.uwbMeas.multiEval.power.msfPower.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	UwbMeas_MultiEval_Power_MsfPower_Average.rst
	UwbMeas_MultiEval_Power_MsfPower_Current.rst
	UwbMeas_MultiEval_Power_MsfPower_Maximum.rst
	UwbMeas_MultiEval_Power_MsfPower_Minimum.rst
	UwbMeas_MultiEval_Power_MsfPower_StandardDev.rst