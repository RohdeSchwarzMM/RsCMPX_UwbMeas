Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:POWer:MSFPower:MAXimum<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:POWer:MSFPower:MAXimum<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:POWer:MSFPower:MAXimum<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:POWer:MSFPower:MAXimum<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Power.MsfPower.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: