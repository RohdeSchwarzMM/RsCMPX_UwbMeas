Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:POWer:MSFPower:MINimum<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:POWer:MSFPower:MINimum<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:POWer:MSFPower:MINimum<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:POWer:MSFPower:MINimum<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Power.MsfPower.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: