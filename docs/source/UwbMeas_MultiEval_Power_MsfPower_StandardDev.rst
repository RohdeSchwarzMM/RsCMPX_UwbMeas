StandardDev
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:POWer:MSFPower:SDEViation<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:POWer:MSFPower:SDEViation<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:POWer:MSFPower:SDEViation<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:POWer:MSFPower:SDEViation<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Power.MsfPower.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: