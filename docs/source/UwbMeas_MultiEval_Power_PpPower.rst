PpPower
----------------------------------------





.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Power.PpPower.PpPowerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.uwbMeas.multiEval.power.ppPower.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	UwbMeas_MultiEval_Power_PpPower_Average.rst
	UwbMeas_MultiEval_Power_PpPower_Current.rst
	UwbMeas_MultiEval_Power_PpPower_Maximum.rst
	UwbMeas_MultiEval_Power_PpPower_Minimum.rst
	UwbMeas_MultiEval_Power_PpPower_StandardDev.rst