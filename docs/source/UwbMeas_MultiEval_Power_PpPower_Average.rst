Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:POWer:PPPower:AVERage<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:POWer:PPPower:AVERage<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:POWer:PPPower:AVERage<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:POWer:PPPower:AVERage<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Power.PpPower.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: