Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:POWer:PPPower:MAXimum<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:POWer:PPPower:MAXimum<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:POWer:PPPower:MAXimum<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:POWer:PPPower:MAXimum<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Power.PpPower.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: