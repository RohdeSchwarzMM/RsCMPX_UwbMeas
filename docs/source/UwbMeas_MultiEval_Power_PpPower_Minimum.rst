Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:POWer:PPPower:MINimum<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:POWer:PPPower:MINimum<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:POWer:PPPower:MINimum<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:POWer:PPPower:MINimum<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Power.PpPower.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: