StandardDev
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:POWer:PPPower:SDEViation<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:POWer:PPPower:SDEViation<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:POWer:PPPower:SDEViation<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:POWer:PPPower:SDEViation<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Power.PpPower.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: