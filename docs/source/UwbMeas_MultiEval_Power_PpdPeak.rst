PpdPeak
----------------------------------------





.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Power.PpdPeak.PpdPeakCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.uwbMeas.multiEval.power.ppdPeak.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	UwbMeas_MultiEval_Power_PpdPeak_Average.rst
	UwbMeas_MultiEval_Power_PpdPeak_Current.rst
	UwbMeas_MultiEval_Power_PpdPeak_Maximum.rst
	UwbMeas_MultiEval_Power_PpdPeak_Minimum.rst
	UwbMeas_MultiEval_Power_PpdPeak_StandardDev.rst