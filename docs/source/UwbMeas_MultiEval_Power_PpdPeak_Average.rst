Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:POWer:PPDPeak:AVERage<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:POWer:PPDPeak:AVERage<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:POWer:PPDPeak:AVERage<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:POWer:PPDPeak:AVERage<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Power.PpdPeak.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: