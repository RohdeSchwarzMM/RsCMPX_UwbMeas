Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:POWer:PPDPeak:MAXimum<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:POWer:PPDPeak:MAXimum<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:POWer:PPDPeak:MAXimum<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:POWer:PPDPeak:MAXimum<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Power.PpdPeak.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: