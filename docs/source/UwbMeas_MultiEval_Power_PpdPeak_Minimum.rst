Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:POWer:PPDPeak:MINimum<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:POWer:PPDPeak:MINimum<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:POWer:PPDPeak:MINimum<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:POWer:PPDPeak:MINimum<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Power.PpdPeak.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: