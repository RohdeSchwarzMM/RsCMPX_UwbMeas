StandardDev
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:POWer:PPDPeak:SDEViation<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:POWer:PPDPeak:SDEViation<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:POWer:PPDPeak:SDEViation<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:POWer:PPDPeak:SDEViation<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Power.PpdPeak.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: