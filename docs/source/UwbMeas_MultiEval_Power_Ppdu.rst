Ppdu<Ppdu>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr100
	rc = driver.uwbMeas.multiEval.power.ppdu.repcap_ppdu_get()
	driver.uwbMeas.multiEval.power.ppdu.repcap_ppdu_set(repcap.Ppdu.Nr1)





.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Power.Ppdu.PpduCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.uwbMeas.multiEval.power.ppdu.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	UwbMeas_MultiEval_Power_Ppdu_Average.rst
	UwbMeas_MultiEval_Power_Ppdu_Current.rst
	UwbMeas_MultiEval_Power_Ppdu_Maximum.rst
	UwbMeas_MultiEval_Power_Ppdu_Minimum.rst
	UwbMeas_MultiEval_Power_Ppdu_StandardDev.rst