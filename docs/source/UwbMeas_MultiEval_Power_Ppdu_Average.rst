Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:POWer:PPDU:AVERage<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:POWer:PPDU:AVERage<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:POWer:PPDU:AVERage<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:POWer:PPDU:AVERage<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Power.Ppdu.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: