Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:POWer:PPDU:CURRent<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:POWer:PPDU:CURRent<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:POWer:PPDU:CURRent<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:POWer:PPDU:CURRent<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Power.Ppdu.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: