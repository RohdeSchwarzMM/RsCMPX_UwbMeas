Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:POWer:PPDU:MAXimum<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:POWer:PPDU:MAXimum<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:POWer:PPDU:MAXimum<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:POWer:PPDU:MAXimum<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Power.Ppdu.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: