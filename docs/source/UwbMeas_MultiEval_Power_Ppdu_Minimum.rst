Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:POWer:PPDU:MINimum<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:POWer:PPDU:MINimum<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:POWer:PPDU:MINimum<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:POWer:PPDU:MINimum<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Power.Ppdu.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: