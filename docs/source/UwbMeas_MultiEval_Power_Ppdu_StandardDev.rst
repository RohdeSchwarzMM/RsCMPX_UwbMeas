StandardDev
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:POWer:PPDU:SDEViation<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:POWer:PPDU:SDEViation<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:POWer:PPDU:SDEViation<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:POWer:PPDU:SDEViation<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Power.Ppdu.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: