Ppower
----------------------------------------





.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Power.Ppower.PpowerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.uwbMeas.multiEval.power.ppower.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	UwbMeas_MultiEval_Power_Ppower_Average.rst
	UwbMeas_MultiEval_Power_Ppower_Current.rst
	UwbMeas_MultiEval_Power_Ppower_Maximum.rst
	UwbMeas_MultiEval_Power_Ppower_Minimum.rst
	UwbMeas_MultiEval_Power_Ppower_StandardDev.rst