Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:POWer:PPOWer:AVERage<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:POWer:PPOWer:AVERage<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:POWer:PPOWer:AVERage<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:POWer:PPOWer:AVERage<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Power.Ppower.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: