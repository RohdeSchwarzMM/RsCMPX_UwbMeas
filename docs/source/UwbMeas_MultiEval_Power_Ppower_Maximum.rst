Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:POWer:PPOWer:MAXimum<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:POWer:PPOWer:MAXimum<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:POWer:PPOWer:MAXimum<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:POWer:PPOWer:MAXimum<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Power.Ppower.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: