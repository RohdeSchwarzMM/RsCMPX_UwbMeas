Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:POWer:PPOWer:MINimum<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:POWer:PPOWer:MINimum<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:POWer:PPOWer:MINimum<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:POWer:PPOWer:MINimum<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Power.Ppower.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: