StandardDev
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:POWer:PPOWer:SDEViation<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:POWer:PPOWer:SDEViation<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:POWer:PPOWer:SDEViation<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:POWer:PPOWer:SDEViation<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Power.Ppower.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: