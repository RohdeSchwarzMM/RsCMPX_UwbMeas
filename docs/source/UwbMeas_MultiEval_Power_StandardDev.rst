StandardDev
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:POWer:SDEViation<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:POWer:SDEViation<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:POWer:SDEViation<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:POWer:SDEViation<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Power.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: