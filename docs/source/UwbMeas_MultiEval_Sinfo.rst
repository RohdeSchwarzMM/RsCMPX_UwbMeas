Sinfo
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:SINFo<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:SINFo<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:SINFo<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:SINFo<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Sinfo.SinfoCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.uwbMeas.multiEval.sinfo.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	UwbMeas_MultiEval_Sinfo_AsSymbols.rst
	UwbMeas_MultiEval_Sinfo_Cindex.rst
	UwbMeas_MultiEval_Sinfo_CsLength.rst
	UwbMeas_MultiEval_Sinfo_Dlength.rst
	UwbMeas_MultiEval_Sinfo_Dppdu.rst
	UwbMeas_MultiEval_Sinfo_Drate.rst
	UwbMeas_MultiEval_Sinfo_FcsCheck.rst
	UwbMeas_MultiEval_Sinfo_Phr.rst
	UwbMeas_MultiEval_Sinfo_Psdu.rst
	UwbMeas_MultiEval_Sinfo_PstGap.rst
	UwbMeas_MultiEval_Sinfo_RaBit.rst
	UwbMeas_MultiEval_Sinfo_ReBit.rst
	UwbMeas_MultiEval_Sinfo_Sfd.rst
	UwbMeas_MultiEval_Sinfo_SfdLength.rst