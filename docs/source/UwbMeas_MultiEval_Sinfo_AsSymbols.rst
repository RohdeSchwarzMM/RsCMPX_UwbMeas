AsSymbols
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:SINFo:ASSYmbols<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:SINFo:ASSYmbols<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:SINFo:ASSYmbols<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:SINFo:ASSYmbols<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Sinfo.AsSymbols.AsSymbolsCls
	:members:
	:undoc-members:
	:noindex: