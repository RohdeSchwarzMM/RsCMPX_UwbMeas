Cindex
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:SINFo:CINDex<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:SINFo:CINDex<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:SINFo:CINDex<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:SINFo:CINDex<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Sinfo.Cindex.CindexCls
	:members:
	:undoc-members:
	:noindex: