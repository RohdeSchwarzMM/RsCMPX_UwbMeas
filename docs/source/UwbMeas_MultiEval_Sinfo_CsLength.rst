CsLength
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:SINFo:CSLength<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:SINFo:CSLength<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:SINFo:CSLength<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:SINFo:CSLength<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Sinfo.CsLength.CsLengthCls
	:members:
	:undoc-members:
	:noindex: