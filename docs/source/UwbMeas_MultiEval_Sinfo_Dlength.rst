Dlength
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:SINFo:DLENgth<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:SINFo:DLENgth<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:SINFo:DLENgth<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:SINFo:DLENgth<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Sinfo.Dlength.DlengthCls
	:members:
	:undoc-members:
	:noindex: