Dppdu
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:SINFo:DPPDu
	single: READ:UWB:MEASurement<Instance>:MEValuation:SINFo:DPPDu

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:SINFo:DPPDu
	READ:UWB:MEASurement<Instance>:MEValuation:SINFo:DPPDu



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Sinfo.Dppdu.DppduCls
	:members:
	:undoc-members:
	:noindex: