Drate
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:SINFo:DRATe<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:SINFo:DRATe<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:SINFo:DRATe<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:SINFo:DRATe<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Sinfo.Drate.DrateCls
	:members:
	:undoc-members:
	:noindex: