FcsCheck
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:SINFo:FCSCheck<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:SINFo:FCSCheck<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:SINFo:FCSCheck<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:SINFo:FCSCheck<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Sinfo.FcsCheck.FcsCheckCls
	:members:
	:undoc-members:
	:noindex: