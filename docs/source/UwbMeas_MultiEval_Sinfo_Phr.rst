Phr
----------------------------------------





.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Sinfo.Phr.PhrCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.uwbMeas.multiEval.sinfo.phr.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	UwbMeas_MultiEval_Sinfo_Phr_AsSymbols.rst
	UwbMeas_MultiEval_Sinfo_Phr_Bitrate.rst
	UwbMeas_MultiEval_Sinfo_Phr_Crc.rst