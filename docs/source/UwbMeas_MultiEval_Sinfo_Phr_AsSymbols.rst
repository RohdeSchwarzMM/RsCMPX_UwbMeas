AsSymbols
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:SINFo:PHR:ASSYmbols<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:SINFo:PHR:ASSYmbols<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:SINFo:PHR:ASSYmbols<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:SINFo:PHR:ASSYmbols<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Sinfo.Phr.AsSymbols.AsSymbolsCls
	:members:
	:undoc-members:
	:noindex: