Bitrate
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:SINFo:PHR:BITRate<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:SINFo:PHR:BITRate<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:SINFo:PHR:BITRate<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:SINFo:PHR:BITRate<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Sinfo.Phr.Bitrate.BitrateCls
	:members:
	:undoc-members:
	:noindex: