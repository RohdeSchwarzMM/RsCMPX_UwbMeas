Crc
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:SINFo:PHR:CRC<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:SINFo:PHR:CRC<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:SINFo:PHR:CRC<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:SINFo:PHR:CRC<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Sinfo.Phr.Crc.CrcCls
	:members:
	:undoc-members:
	:noindex: