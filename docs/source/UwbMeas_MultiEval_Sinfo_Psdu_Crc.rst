Crc
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:SINFo:PSDU:CRC<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:SINFo:PSDU:CRC<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:SINFo:PSDU:CRC<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:SINFo:PSDU:CRC<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Sinfo.Psdu.Crc.CrcCls
	:members:
	:undoc-members:
	:noindex: