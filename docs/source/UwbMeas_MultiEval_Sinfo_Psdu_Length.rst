Length
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:SINFo:PSDU:LENGth<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:SINFo:PSDU:LENGth<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:SINFo:PSDU:LENGth<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:SINFo:PSDU:LENGth<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Sinfo.Psdu.Length.LengthCls
	:members:
	:undoc-members:
	:noindex: