PstGap
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:UWB:MEASurement<Instance>:MEValuation:SINFo:PSTGap<PPDU>
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:SINFo:PSTGap<PPDU>

.. code-block:: python

	READ:UWB:MEASurement<Instance>:MEValuation:SINFo:PSTGap<PPDU>
	FETCh:UWB:MEASurement<Instance>:MEValuation:SINFo:PSTGap<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Sinfo.PstGap.PstGapCls
	:members:
	:undoc-members:
	:noindex: