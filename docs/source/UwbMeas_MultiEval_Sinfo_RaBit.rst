RaBit
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:SINFo:RABit<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:SINFo:RABit<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:SINFo:RABit<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:SINFo:RABit<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Sinfo.RaBit.RaBitCls
	:members:
	:undoc-members:
	:noindex: