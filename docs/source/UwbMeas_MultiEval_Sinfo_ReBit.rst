ReBit
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:SINFo:REBit<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:SINFo:REBit<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:SINFo:REBit<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:SINFo:REBit<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Sinfo.ReBit.ReBitCls
	:members:
	:undoc-members:
	:noindex: