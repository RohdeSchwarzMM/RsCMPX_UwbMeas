Sfd
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:SINFo:SFD<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:SINFo:SFD<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:SINFo:SFD<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:SINFo:SFD<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Sinfo.Sfd.SfdCls
	:members:
	:undoc-members:
	:noindex: