SfdLength
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:SINFo:SFDLength<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:SINFo:SFDLength<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:SINFo:SFDLength<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:SINFo:SFDLength<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Sinfo.SfdLength.SfdLengthCls
	:members:
	:undoc-members:
	:noindex: