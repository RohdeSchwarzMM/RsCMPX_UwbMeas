State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:STATe

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:STATe



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.State.StateCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.uwbMeas.multiEval.state.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	UwbMeas_MultiEval_State_All.rst