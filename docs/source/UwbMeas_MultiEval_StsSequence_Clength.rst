Clength
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:STSSequence:CLENgth<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:STSSequence:CLENgth<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:STSSequence:CLENgth<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:STSSequence:CLENgth<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.StsSequence.Clength.ClengthCls
	:members:
	:undoc-members:
	:noindex: