Content
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:STSSequence:CONTent<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:STSSequence:CONTent<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:STSSequence:CONTent<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:STSSequence:CONTent<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.StsSequence.Content.ContentCls
	:members:
	:undoc-members:
	:noindex: