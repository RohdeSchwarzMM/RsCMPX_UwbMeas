Trace
----------------------------------------





.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Trace.TraceCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.uwbMeas.multiEval.trace.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	UwbMeas_MultiEval_Trace_CpJitter.rst
	UwbMeas_MultiEval_Trace_CtJitter.rst
	UwbMeas_MultiEval_Trace_NcCorr.rst
	UwbMeas_MultiEval_Trace_Pmask.rst
	UwbMeas_MultiEval_Trace_PowerVsTime.rst
	UwbMeas_MultiEval_Trace_Sbwl.rst
	UwbMeas_MultiEval_Trace_Sbws.rst
	UwbMeas_MultiEval_Trace_SpJitter.rst
	UwbMeas_MultiEval_Trace_StJitter.rst
	UwbMeas_MultiEval_Trace_TsMask.rst