Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:TRACe:CPJitter:AVERage
	single: READ:UWB:MEASurement<Instance>:MEValuation:TRACe:CPJitter:AVERage

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:TRACe:CPJitter:AVERage
	READ:UWB:MEASurement<Instance>:MEValuation:TRACe:CPJitter:AVERage



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Trace.CpJitter.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: