Xvalues
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:TRACe:CPJitter:XVALues
	single: READ:UWB:MEASurement<Instance>:MEValuation:TRACe:CPJitter:XVALues

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:TRACe:CPJitter:XVALues
	READ:UWB:MEASurement<Instance>:MEValuation:TRACe:CPJitter:XVALues



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Trace.CpJitter.Xvalues.XvaluesCls
	:members:
	:undoc-members:
	:noindex: