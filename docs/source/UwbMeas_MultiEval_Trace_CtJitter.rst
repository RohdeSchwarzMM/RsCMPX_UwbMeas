CtJitter
----------------------------------------





.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Trace.CtJitter.CtJitterCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.uwbMeas.multiEval.trace.ctJitter.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	UwbMeas_MultiEval_Trace_CtJitter_Average.rst
	UwbMeas_MultiEval_Trace_CtJitter_Current.rst
	UwbMeas_MultiEval_Trace_CtJitter_Xvalues.rst