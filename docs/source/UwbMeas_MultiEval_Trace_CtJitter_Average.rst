Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:TRACe:CTJitter:AVERage
	single: READ:UWB:MEASurement<Instance>:MEValuation:TRACe:CTJitter:AVERage

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:TRACe:CTJitter:AVERage
	READ:UWB:MEASurement<Instance>:MEValuation:TRACe:CTJitter:AVERage



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Trace.CtJitter.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: