Xvalues
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:TRACe:CTJitter:XVALues
	single: READ:UWB:MEASurement<Instance>:MEValuation:TRACe:CTJitter:XVALues

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:TRACe:CTJitter:XVALues
	READ:UWB:MEASurement<Instance>:MEValuation:TRACe:CTJitter:XVALues



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Trace.CtJitter.Xvalues.XvaluesCls
	:members:
	:undoc-members:
	:noindex: