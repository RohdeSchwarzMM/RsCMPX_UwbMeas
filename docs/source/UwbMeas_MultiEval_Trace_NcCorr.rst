NcCorr
----------------------------------------





.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Trace.NcCorr.NcCorrCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.uwbMeas.multiEval.trace.ncCorr.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	UwbMeas_MultiEval_Trace_NcCorr_Average.rst
	UwbMeas_MultiEval_Trace_NcCorr_Current.rst
	UwbMeas_MultiEval_Trace_NcCorr_Xvalues.rst