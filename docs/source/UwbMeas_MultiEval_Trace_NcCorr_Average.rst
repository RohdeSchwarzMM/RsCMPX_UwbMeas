Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:TRACe:NCCorr:AVERage
	single: READ:UWB:MEASurement<Instance>:MEValuation:TRACe:NCCorr:AVERage

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:TRACe:NCCorr:AVERage
	READ:UWB:MEASurement<Instance>:MEValuation:TRACe:NCCorr:AVERage



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Trace.NcCorr.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: