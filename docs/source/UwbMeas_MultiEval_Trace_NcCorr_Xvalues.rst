Xvalues
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:TRACe:NCCorr:XVALues
	single: READ:UWB:MEASurement<Instance>:MEValuation:TRACe:NCCorr:XVALues

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:TRACe:NCCorr:XVALues
	READ:UWB:MEASurement<Instance>:MEValuation:TRACe:NCCorr:XVALues



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Trace.NcCorr.Xvalues.XvaluesCls
	:members:
	:undoc-members:
	:noindex: