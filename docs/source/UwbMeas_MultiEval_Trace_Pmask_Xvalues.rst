Xvalues
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:TRACe:PMASk:XVALues
	single: READ:UWB:MEASurement<Instance>:MEValuation:TRACe:PMASk:XVALues

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:TRACe:PMASk:XVALues
	READ:UWB:MEASurement<Instance>:MEValuation:TRACe:PMASk:XVALues



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Trace.Pmask.Xvalues.XvaluesCls
	:members:
	:undoc-members:
	:noindex: