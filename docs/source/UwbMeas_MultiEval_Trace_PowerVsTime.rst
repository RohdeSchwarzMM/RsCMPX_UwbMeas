PowerVsTime
----------------------------------------





.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Trace.PowerVsTime.PowerVsTimeCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.uwbMeas.multiEval.trace.powerVsTime.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	UwbMeas_MultiEval_Trace_PowerVsTime_Maximum.rst
	UwbMeas_MultiEval_Trace_PowerVsTime_Minimum.rst
	UwbMeas_MultiEval_Trace_PowerVsTime_Xvalues.rst