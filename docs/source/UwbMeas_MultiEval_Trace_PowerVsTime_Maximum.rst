Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:TRACe:PVTime:MAXimum<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:TRACe:PVTime:MAXimum<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:TRACe:PVTime:MAXimum<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:TRACe:PVTime:MAXimum<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Trace.PowerVsTime.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: