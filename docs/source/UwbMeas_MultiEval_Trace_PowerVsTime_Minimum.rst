Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:TRACe:PVTime:MINimum<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:TRACe:PVTime:MINimum<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:TRACe:PVTime:MINimum<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:TRACe:PVTime:MINimum<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Trace.PowerVsTime.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: