Xvalues
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:TRACe:PVTime:XVALues
	single: READ:UWB:MEASurement<Instance>:MEValuation:TRACe:PVTime:XVALues

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:TRACe:PVTime:XVALues
	READ:UWB:MEASurement<Instance>:MEValuation:TRACe:PVTime:XVALues



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Trace.PowerVsTime.Xvalues.XvaluesCls
	:members:
	:undoc-members:
	:noindex: