Sbwl
----------------------------------------





.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Trace.Sbwl.SbwlCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.uwbMeas.multiEval.trace.sbwl.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	UwbMeas_MultiEval_Trace_Sbwl_Average.rst
	UwbMeas_MultiEval_Trace_Sbwl_Current.rst
	UwbMeas_MultiEval_Trace_Sbwl_Xvalues.rst