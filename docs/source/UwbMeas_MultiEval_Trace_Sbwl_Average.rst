Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:TRACe:SBWL:AVERage
	single: READ:UWB:MEASurement<Instance>:MEValuation:TRACe:SBWL:AVERage

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:TRACe:SBWL:AVERage
	READ:UWB:MEASurement<Instance>:MEValuation:TRACe:SBWL:AVERage



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Trace.Sbwl.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: