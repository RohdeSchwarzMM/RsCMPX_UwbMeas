Current<Ppdu>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr100
	rc = driver.uwbMeas.multiEval.trace.sbwl.current.repcap_ppdu_get()
	driver.uwbMeas.multiEval.trace.sbwl.current.repcap_ppdu_set(repcap.Ppdu.Nr1)



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:TRACe:SBWL:CURRent<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:TRACe:SBWL:CURRent<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:TRACe:SBWL:CURRent<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:TRACe:SBWL:CURRent<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Trace.Sbwl.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.uwbMeas.multiEval.trace.sbwl.current.clone()