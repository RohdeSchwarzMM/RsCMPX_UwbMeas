Xvalues
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:TRACe:SBWL:XVALues
	single: READ:UWB:MEASurement<Instance>:MEValuation:TRACe:SBWL:XVALues

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:TRACe:SBWL:XVALues
	READ:UWB:MEASurement<Instance>:MEValuation:TRACe:SBWL:XVALues



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Trace.Sbwl.Xvalues.XvaluesCls
	:members:
	:undoc-members:
	:noindex: