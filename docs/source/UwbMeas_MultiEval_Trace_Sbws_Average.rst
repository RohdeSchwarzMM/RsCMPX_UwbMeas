Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:TRACe:SBWS:AVERage
	single: READ:UWB:MEASurement<Instance>:MEValuation:TRACe:SBWS:AVERage

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:TRACe:SBWS:AVERage
	READ:UWB:MEASurement<Instance>:MEValuation:TRACe:SBWS:AVERage



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Trace.Sbws.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: