Xvalues
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:TRACe:SBWS:XVALues
	single: READ:UWB:MEASurement<Instance>:MEValuation:TRACe:SBWS:XVALues

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:TRACe:SBWS:XVALues
	READ:UWB:MEASurement<Instance>:MEValuation:TRACe:SBWS:XVALues



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Trace.Sbws.Xvalues.XvaluesCls
	:members:
	:undoc-members:
	:noindex: