Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:TRACe:SPJitter:AVERage
	single: READ:UWB:MEASurement<Instance>:MEValuation:TRACe:SPJitter:AVERage

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:TRACe:SPJitter:AVERage
	READ:UWB:MEASurement<Instance>:MEValuation:TRACe:SPJitter:AVERage



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Trace.SpJitter.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: