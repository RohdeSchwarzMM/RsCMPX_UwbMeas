Xvalues
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:TRACe:SPJitter:XVALues
	single: READ:UWB:MEASurement<Instance>:MEValuation:TRACe:SPJitter:XVALues

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:TRACe:SPJitter:XVALues
	READ:UWB:MEASurement<Instance>:MEValuation:TRACe:SPJitter:XVALues



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Trace.SpJitter.Xvalues.XvaluesCls
	:members:
	:undoc-members:
	:noindex: