StJitter
----------------------------------------





.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Trace.StJitter.StJitterCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.uwbMeas.multiEval.trace.stJitter.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	UwbMeas_MultiEval_Trace_StJitter_Average.rst
	UwbMeas_MultiEval_Trace_StJitter_Current.rst
	UwbMeas_MultiEval_Trace_StJitter_Xvalues.rst