Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:TRACe:STJitter:AVERage
	single: READ:UWB:MEASurement<Instance>:MEValuation:TRACe:STJitter:AVERage

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:TRACe:STJitter:AVERage
	READ:UWB:MEASurement<Instance>:MEValuation:TRACe:STJitter:AVERage



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Trace.StJitter.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: