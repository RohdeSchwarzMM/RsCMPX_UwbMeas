Xvalues
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:TRACe:STJitter:XVALues
	single: READ:UWB:MEASurement<Instance>:MEValuation:TRACe:STJitter:XVALues

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:TRACe:STJitter:XVALues
	READ:UWB:MEASurement<Instance>:MEValuation:TRACe:STJitter:XVALues



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Trace.StJitter.Xvalues.XvaluesCls
	:members:
	:undoc-members:
	:noindex: