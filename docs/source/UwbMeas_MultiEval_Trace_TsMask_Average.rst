Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:TRACe:TSMask:AVERage
	single: READ:UWB:MEASurement<Instance>:MEValuation:TRACe:TSMask:AVERage

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:TRACe:TSMask:AVERage
	READ:UWB:MEASurement<Instance>:MEValuation:TRACe:TSMask:AVERage



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Trace.TsMask.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: