Xvalues
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:TRACe:TSMask:XVALues
	single: READ:UWB:MEASurement<Instance>:MEValuation:TRACe:TSMask:XVALues

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:TRACe:TSMask:XVALues
	READ:UWB:MEASurement<Instance>:MEValuation:TRACe:TSMask:XVALues



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.Trace.TsMask.Xvalues.XvaluesCls
	:members:
	:undoc-members:
	:noindex: