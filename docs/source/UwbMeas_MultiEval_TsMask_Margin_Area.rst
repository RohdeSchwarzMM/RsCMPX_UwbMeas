Area<Area>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr3
	rc = driver.uwbMeas.multiEval.tsMask.margin.area.repcap_area_get()
	driver.uwbMeas.multiEval.tsMask.margin.area.repcap_area_set(repcap.Area.Nr1)





.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.TsMask.Margin.Area.AreaCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.uwbMeas.multiEval.tsMask.margin.area.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	UwbMeas_MultiEval_TsMask_Margin_Area_Average.rst
	UwbMeas_MultiEval_TsMask_Margin_Area_Current.rst
	UwbMeas_MultiEval_TsMask_Margin_Area_Negativ.rst
	UwbMeas_MultiEval_TsMask_Margin_Area_Positiv.rst