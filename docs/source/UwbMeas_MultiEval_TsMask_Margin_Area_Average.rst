Average
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CALCulate:UWB:MEASurement<Instance>:MEValuation:TSMask:MARGin:AREA<nr>:AVERage<PPDU>

.. code-block:: python

	CALCulate:UWB:MEASurement<Instance>:MEValuation:TSMask:MARGin:AREA<nr>:AVERage<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.TsMask.Margin.Area.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: