Negativ
----------------------------------------





.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.TsMask.Margin.Area.Negativ.NegativCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.uwbMeas.multiEval.tsMask.margin.area.negativ.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	UwbMeas_MultiEval_TsMask_Margin_Area_Negativ_Average.rst
	UwbMeas_MultiEval_TsMask_Margin_Area_Negativ_Current.rst