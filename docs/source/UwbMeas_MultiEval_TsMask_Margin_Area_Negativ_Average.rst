Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:TSMask:MARGin:AREA<nr>:NEGativ:AVERage<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:TSMask:MARGin:AREA<nr>:NEGativ:AVERage<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:TSMask:MARGin:AREA<nr>:NEGativ:AVERage<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:TSMask:MARGin:AREA<nr>:NEGativ:AVERage<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.TsMask.Margin.Area.Negativ.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: