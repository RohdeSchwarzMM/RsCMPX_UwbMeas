Otolerance
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:TSMask:OTOLerance
	single: READ:UWB:MEASurement<Instance>:MEValuation:TSMask:OTOLerance

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:TSMask:OTOLerance
	READ:UWB:MEASurement<Instance>:MEValuation:TSMask:OTOLerance



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.TsMask.Otolerance.OtoleranceCls
	:members:
	:undoc-members:
	:noindex: