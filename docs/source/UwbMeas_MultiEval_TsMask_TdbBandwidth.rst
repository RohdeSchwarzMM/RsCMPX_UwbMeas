TdbBandwidth
----------------------------------------





.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.TsMask.TdbBandwidth.TdbBandwidthCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.uwbMeas.multiEval.tsMask.tdbBandwidth.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	UwbMeas_MultiEval_TsMask_TdbBandwidth_Frequency.rst
	UwbMeas_MultiEval_TsMask_TdbBandwidth_PsPower.rst