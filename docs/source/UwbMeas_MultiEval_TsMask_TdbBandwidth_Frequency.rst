Frequency
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:TSMask:TDBBandwidth:FREQuency<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:TSMask:TDBBandwidth:FREQuency<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:TSMask:TDBBandwidth:FREQuency<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:TSMask:TDBBandwidth:FREQuency<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.TsMask.TdbBandwidth.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.uwbMeas.multiEval.tsMask.tdbBandwidth.frequency.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	UwbMeas_MultiEval_TsMask_TdbBandwidth_Frequency_Fhfl.rst