Fhfl
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CALCulate:UWB:MEASurement<Instance>:MEValuation:TSMask:TDBBandwidth:FREQuency:FHFL<PPDU>

.. code-block:: python

	CALCulate:UWB:MEASurement<Instance>:MEValuation:TSMask:TDBBandwidth:FREQuency:FHFL<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.TsMask.TdbBandwidth.Frequency.Fhfl.FhflCls
	:members:
	:undoc-members:
	:noindex: