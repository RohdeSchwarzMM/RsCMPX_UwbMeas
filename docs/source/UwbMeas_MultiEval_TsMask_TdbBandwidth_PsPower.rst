PsPower
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:UWB:MEASurement<Instance>:MEValuation:TSMask:TDBBandwidth:PSPower<PPDU>
	single: READ:UWB:MEASurement<Instance>:MEValuation:TSMask:TDBBandwidth:PSPower<PPDU>

.. code-block:: python

	FETCh:UWB:MEASurement<Instance>:MEValuation:TSMask:TDBBandwidth:PSPower<PPDU>
	READ:UWB:MEASurement<Instance>:MEValuation:TSMask:TDBBandwidth:PSPower<PPDU>



.. autoclass:: RsCMPX_UwbMeas.Implementations.UwbMeas.MultiEval.TsMask.TdbBandwidth.PsPower.PsPowerCls
	:members:
	:undoc-members:
	:noindex: