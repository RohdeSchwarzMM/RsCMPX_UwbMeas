Enums
=========

Connector
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.Connector.I11I
	# Last value:
	value = enums.Connector.RH8
	# All values (163x):
	I11I | I13I | I15I | I17I | I21I | I23I | I25I | I27I
	I31I | I33I | I35I | I37I | I41I | I43I | I45I | I47I
	IFI1 | IFI2 | IFI3 | IFI4 | IFI5 | IFI6 | IQ1I | IQ3I
	IQ5I | IQ7I | R10D | R11 | R11C | R11D | R12 | R12C
	R12D | R12I | R13 | R13C | R14 | R14C | R14I | R15
	R16 | R17 | R18 | R21 | R21C | R22 | R22C | R22I
	R23 | R23C | R24 | R24C | R24I | R25 | R26 | R27
	R28 | R31 | R31C | R32 | R32C | R32I | R33 | R33C
	R34 | R34C | R34I | R35 | R36 | R37 | R38 | R41
	R41C | R42 | R42C | R42I | R43 | R43C | R44 | R44C
	R44I | R45 | R46 | R47 | R48 | RA1 | RA2 | RA3
	RA4 | RA5 | RA6 | RA7 | RA8 | RB1 | RB2 | RB3
	RB4 | RB5 | RB6 | RB7 | RB8 | RC1 | RC2 | RC3
	RC4 | RC5 | RC6 | RC7 | RC8 | RD1 | RD2 | RD3
	RD4 | RD5 | RD6 | RD7 | RD8 | RE1 | RE2 | RE3
	RE4 | RE5 | RE6 | RE7 | RE8 | RF1 | RF1C | RF2
	RF2C | RF2I | RF3 | RF3C | RF4 | RF4C | RF4I | RF5
	RF5C | RF6 | RF6C | RF7 | RF7C | RF8 | RF8C | RF9C
	RFAC | RFBC | RFBI | RG1 | RG2 | RG3 | RG4 | RG5
	RG6 | RG7 | RG8 | RH1 | RH2 | RH3 | RH4 | RH5
	RH6 | RH7 | RH8

MaxSpecPowLen
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MaxSpecPowLen.MS1
	# All values (2x):
	MS1 | PPDU

PhrDataRate
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PhrDataRate.DRHP
	# All values (8x):
	DRHP | DRLP | DRMD | IGN | RHMH | RHML | RSF | SYNC

PpduMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PpduMode.MPPDu
	# All values (2x):
	MPPDu | SPPDu

Repeat
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Repeat.CONTinuous
	# All values (2x):
	CONTinuous | SINGleshot

ResourceState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ResourceState.ACTive
	# All values (8x):
	ACTive | ADJusted | INValid | OFF | PENDing | QUEued | RDY | RUN

Result
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Result.FAIL
	# All values (2x):
	FAIL | PASS

ResultStatus2
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.ResultStatus2.DC
	# Last value:
	value = enums.ResultStatus2.ULEU
	# All values (10x):
	DC | INV | NAV | NCAP | OFF | OFL | OK | UFL
	ULEL | ULEU

SignalSlope
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SignalSlope.FEDGe
	# All values (2x):
	FEDGe | REDGe

StopCondition
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.StopCondition.NONE
	# All values (2x):
	NONE | SLFail

StsSegmentLen
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.StsSegmentLen.L128
	# All values (4x):
	L128 | L256 | L32 | L64

TargetMainState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TargetMainState.OFF
	# All values (3x):
	OFF | RDY | RUN

TargetSyncState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TargetSyncState.ADJusted
	# All values (2x):
	ADJusted | PENDing

